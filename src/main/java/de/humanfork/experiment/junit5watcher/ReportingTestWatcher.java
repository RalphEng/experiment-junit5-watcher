package de.humanfork.experiment.junit5watcher;

import java.util.Optional;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;
import org.junit.jupiter.api.extension.TestWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportingTestWatcher implements TestWatcher, TestExecutionExceptionHandler {

    private Logger LOGGER = LoggerFactory.getLogger(ReportingTestWatcher.class);

    public ReportingTestWatcher() {
        LOGGER.info("init ReportingTestWatcher");
    }

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        LOGGER.info("TestWatcher:testDisabled context={} reason={}", UnitUtils.shortDescription(context), reason);
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        LOGGER.info("TestWatcher:testSuccessful context={}", UnitUtils.shortDescription(context));
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        LOGGER.info("TestWatcher:testAborted context={} reason={}", UnitUtils.shortDescription(context), shortDescription(cause));
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        LOGGER.info("TestWatcher:testFailed context={} reason={}", UnitUtils.shortDescription(context), shortDescription(cause));
    }

    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        LOGGER.info("TestExecutionExceptionHandler:handleTestExecutionException context={} throwable={}",
                UnitUtils.shortDescription(context),
                shortDescription(throwable));
        //it is important to rethrow the exception, else it is swallowed (that is because the method is name HANDLE)
        throw (throwable);        
    }

    private static String shortDescription(Throwable t) {
        return t.getClass().getName() + " " + t.getMessage() + " " + t.getStackTrace()[0];
    }

}
