package de.humanfork.experiment.junit5watcher;

import org.junit.jupiter.api.extension.ExtensionContext;

public class UnitUtils {

    
    static String shortDescription(ExtensionContext context) {
        return context.getDisplayName() + " " + context.getTestClass() + " " + context.getTestMethod();
    }

}
