package de.humanfork.experiment.junit5watcher;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * https://junit.org/junit5/docs/current/user-guide/#extensions-lifecycle-callbacks-before-after-execution
 * 
 * BeforeTestExecutionCallback and AfterTestExecutionCallback define the APIs for Extensions that wish to add behavior that
 * will be executed immediately before and immediately after a test method is executed, respectively.
 * As such, these callbacks are well suited for timing, tracing, and similar use cases. 
 * 
 * If you need to implement callbacks that are invoked around @BeforeEach and @AfterEach methods, implement BeforeEachCallback and
 * AfterEachCallback instead.
 * 
 */
public class AfterCallback implements AfterAllCallback, AfterEachCallback, AfterTestExecutionCallback {

    private Logger LOGGER = LoggerFactory.getLogger(AfterCallback.class);

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        LOGGER.info("AfterAllCallback:afterAll context={}");

    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        LOGGER.info("AfterAllCallback:afterEach context={}");
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        LOGGER.info("AfterAllCallback:afterTestExecution context={}");
    }

}
