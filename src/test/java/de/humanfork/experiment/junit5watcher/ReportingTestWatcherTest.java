package de.humanfork.experiment.junit5watcher;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ralph Engelmann
 */
@ExtendWith(value = { ReportingTestWatcher.class, AfterCallback.class })
public class ReportingTestWatcherTest {

    private Logger LOGGER = LoggerFactory.getLogger(ReportingTestWatcherTest.class);

    @Test
    public void testSuccess() {
        LOGGER.info("execute example test successfull");
    }

    @Test
    public void testFail() {
        LOGGER.info("execute example test fail");

        assertTrue(false);
    }

    @Test
    public void testRuntimeException() {
        LOGGER.info("execute example test with runtime exception");

        throw new RuntimeException("because it is a test");
    }
}
