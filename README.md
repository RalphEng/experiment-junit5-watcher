Test JUnit 5.4 Test-Watcher
===========================

JUnit Test Watcher, defines the API for Extensions that wish to process test results.
They are intoduced in JUnit 5.4.
(There was a old Test Watcher concept in JUnit 4 too.)

Interface `TestWatcher`
https://junit.org/junit5/docs/current/api/org/junit/jupiter/api/extension/TestWatcher.html


[Junit 5.4 Release Notes](https://junit.org/junit5/docs/snapshot/release-notes/index.html#release-notes-5.4.0)